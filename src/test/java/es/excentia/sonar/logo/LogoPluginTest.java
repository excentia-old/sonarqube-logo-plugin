/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
/**
 * 
 */
package es.excentia.sonar.logo;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

/**
 * 
 * Tests plugin class. Just check if there are four extensions assembled in the plugin.
 * 
 * @author acalero
 * 
 */
public class LogoPluginTest {

  private final LogoPlugin plugin = new LogoPlugin();

  /**
   * Test method for {@link es.excentia.sonar.logo.LogoPlugin#getExtensions()}.
   */
  @Test
  public void testGetExtensions() throws Exception {
    assertEquals("Plugin extensions must contain all extensions created", plugin.getExtensions(),
        Arrays.asList(LogoPostJob.class, LogoMetrics.class, LogoWebService.class, LogoDashboardWidget.class));
  }
}