/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
/**
 * 
 */
package es.excentia.sonar.logo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests that the resource for the widget exists.
 * 
 * @author acalero
 * 
 */
public class LogoDashboardWidgetTest {

  private LogoDashboardWidget widget = new LogoDashboardWidget();

  @Test
  public void testWidgetId() {
    assertEquals(widget.getId(), "logo");
  }

  @Test
  public void testWidgetTitle() {
    assertEquals(widget.getTitle(), "Logo");
  }

  @Test
  public void testWidgetTemplatePath() {
    assertEquals(widget.getTemplatePath(), "/es/excentia/sonar/logo/logo_dashboard_widget.html.erb");
  }
}