/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
/**
 * 
 */
package es.excentia.sonar.logo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.sonar.api.config.Settings;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.ProjectFileSystem;

/**
 * @author acalero
 * 
 */
public class LogoPostJobTest {

  private Settings settings = new Settings();
  private Project project;
  private LogoPostJob postJob;
  private ByteArrayInputStream inputStream;
  private ByteArrayOutputStream outputStream;
  private byte[] byteArrayInput = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };

  /**
   * Setting up the test
   */
  @Before
  public void setUp() {
    project = mock(Project.class);

    postJob = new LogoPostJob(settings);

    inputStream = new ByteArrayInputStream(byteArrayInput);
    outputStream = new ByteArrayOutputStream(byteArrayInput.length);
  }

  @Test
  public void testShouldExecuteOnProject() throws Exception {
    assertTrue("Always true", postJob.shouldExecuteOnProject(project));
  }

  @Test
  public void testCopy() {
    try {
      postJob.copy(inputStream, outputStream);
    } catch (Exception exception) {
      assertTrue(exception instanceof IOException);
    }

    byte[] byteArrayOutput = outputStream.toByteArray();
    assertTrue(Arrays.equals(byteArrayInput, byteArrayOutput));
  }

  @Test
  public void testLoadFile() {
    ProjectFileSystem projectFileSystem = mock(ProjectFileSystem.class);
    File baseDir = new File(".");

    when(project.getFileSystem()).thenReturn(projectFileSystem);
    when(projectFileSystem.getBasedir()).thenReturn(baseDir);

    byte[] imageByteArray = null;

    try {
      // test real image
      imageByteArray = postJob.loadFile("sources:/src/main/site/logo.png", project);
      assertTrue(imageByteArray.length > 0);

      // fake image to check that if we use URL the metric will be null
      imageByteArray = postJob.loadFile("http://mytest/test.png", project);
      assertSame(imageByteArray.length, 0);

      // test image that doesn't exists
      imageByteArray = postJob.loadFile("sources:/src/image/not/found.png", project);

    } catch (Exception exception) {
      assertTrue(exception instanceof IOException);
    }
  }

  @Test
  public void testGetImgExt() {
    settings.setProperty(LogoPluginConst.CONFKEY_IMG_LOCATION, "");

    String fileExtension = postJob.getImgExt();
    assertNull(fileExtension);

    settings.setProperty(LogoPluginConst.CONFKEY_IMG_LOCATION, "http://mytest/test.png");

    fileExtension = postJob.getImgExt();
    assertEquals(fileExtension, "png");
  }
}