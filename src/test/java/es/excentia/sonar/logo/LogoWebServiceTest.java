/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
/**
 * 
 */
package es.excentia.sonar.logo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests LogoWebService class. Just check that template path is not null.
 * 
 * @author acalero
 * 
 */
public class LogoWebServiceTest {

  private LogoWebService service = new LogoWebService();

  @Test
  public void testServiceId() {
    assertEquals(service.getId(), "logo");
  }

  @Test
  public void testServiceTemplatePath() {
    assertEquals(service.getTemplatePath(), "/es/excentia/sonar/logo/LogoController.rb");
  }
}