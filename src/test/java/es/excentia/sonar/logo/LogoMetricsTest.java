/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
/**
 * 
 */
package es.excentia.sonar.logo;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

/**
 * Metrics unit test. Just check that there are two metrics.
 * 
 * @author acalero
 * 
 */
public class LogoMetricsTest {

  private final LogoMetrics metrics = new LogoMetrics();

  @Test
  public void testGetMetrics() {
    assertEquals("Metrics list must contain all metrics created", metrics.getMetrics(),
        Arrays.asList(LogoMetrics.LOGO_DATA, LogoMetrics.LOGO_EXT));
  }
}