/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.logo;

import java.util.ArrayList;
import java.util.List;

import org.sonar.api.Extension;
import org.sonar.api.Properties;
import org.sonar.api.Property;
import org.sonar.api.SonarPlugin;

/**
 * Logo plugin
 */
@Properties({
  @Property(key = LogoPluginConst.CONFKEY_IMG_LOCATION, name = "Image location", description = "Image location inside the sources or via URL (ex: sources:/src/main/site/logo.png or http://www.myproj.org/img/logo.png)", project = true, global = true),
  @Property(key = LogoPluginConst.CONFKEY_IMG_STYLE, name = "Image css style", description = "Image css style (ex: width:200px; display: block; margin: auto;)", project = true, global = true),
  @Property(key = LogoPluginConst.CONFKEY_LINK, name = "Project url", description = "Project url", project = true, global = true),
  @Property(key = LogoPluginConst.CONFKEY_ALT_TEXT, name = "Alternative text", description = "Alternative text", project = true, global = true) })
public class LogoPlugin extends SonarPlugin {

  /**
   * Returns Classes to use into the plugin List
   * 
   * @return the classes to use into the plugin List
   */
  public final List<Class<? extends Extension>> getExtensions() {
    List<Class<? extends Extension>> extensions = new ArrayList<Class<? extends Extension>>();

    // Add extensions
    extensions.add(LogoPostJob.class);
    extensions.add(LogoMetrics.class);
    extensions.add(LogoWebService.class);
    extensions.add(LogoDashboardWidget.class);

    return extensions;
  }
}