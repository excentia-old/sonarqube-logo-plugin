/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.logo;

import org.sonar.api.web.AbstractRubyTemplate;
import org.sonar.api.web.RubyRailsWebservice;

/**
 * Web Service at sonarhost/api/plugins/Logo/getLogo?project=[project]
 */
public class LogoWebService extends AbstractRubyTemplate implements RubyRailsWebservice {

  /**
   * {@inheritDoc}
   */
  @Override
  public final String getTemplatePath() {
    return "/es/excentia/sonar/logo/LogoController.rb";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public final String getId() {
    return "logo";
  }
}