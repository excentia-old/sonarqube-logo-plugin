/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.logo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.CheckProject;
import org.sonar.api.batch.PostJob;
import org.sonar.api.batch.SensorContext;
import org.sonar.api.config.Settings;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.PersistenceMode;
import org.sonar.api.resources.Project;

/**
 * This is the post job in Sonar that store the image as a metric
 */
public class LogoPostJob implements PostJob, CheckProject {

  private static final Logger LOG = LoggerFactory.getLogger(LogoPostJob.class);
  private static final Integer SIZE = 1024;

  private final Settings settings;

  /**
   * Constructor
   * 
   * @param settings
   *          Sonar configuration
   */
  public LogoPostJob(Settings settings) {
    this.settings = settings;
  }

  /**
   * Enable execution on project
   */
  @Override
  public final boolean shouldExecuteOnProject(Project project) {
    return true;
  }

  /**
   * Gets all the properties and stores the image in the metric
   */
  @Override
  public final void executeOn(Project project, SensorContext context) {
    String imgLocation = settings.getString(LogoPluginConst.CONFKEY_IMG_LOCATION);
    String imgExt = getImgExt();

    String data = null;

    if ( !StringUtils.isBlank(imgLocation)) {
      if (imgExt == null) {
        LOG.error("Logo image file must have an extension: " + imgLocation);
      } else {
        try {
          // load image data into an String because we need to save a measure
          data = new String(loadFile(imgLocation, project));

        } catch (FileNotFoundException exception) {
          LOG.error("Couldn't find image: " + imgLocation);
        } catch (Exception exception) {
          LOG.error("Couldn't load image: " + imgLocation, exception);
        }
      }
    }

    // Save measures
    Measure measureData = new Measure(LogoMetrics.LOGO_DATA);
    measureData.setPersistenceMode(PersistenceMode.DATABASE);
    measureData.setData(data);
    context.saveMeasure(measureData);

    Measure measureExt = new Measure(LogoMetrics.LOGO_EXT);
    measureExt.setData(imgExt);
    context.saveMeasure(measureExt);
  }

  /**
   * Copy bytes from input stream to output stream
   */
  protected final void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
    byte[] barr = new byte[SIZE];

    while (true) {
      int bytesRead = inputStream.read(barr);
      if (bytesRead <= 0) {
        break;
      }
      outputStream.write(barr, 0, bytesRead);
    }
  }

  /**
   * Load file into a byte an encoded base64 byte array
   */
  protected final byte[] loadFile(String imgLocation, Project project) throws IOException {
    byte[] logoByteArray = new byte[0];

    // We will load the image only if it is in the sources
    if (imgLocation.startsWith(LogoPluginConst.IMGPATH_PREFIX_SOURCES)) {
      String baseDirPath = project.getFileSystem().getBasedir().getAbsolutePath();
      String relativePath = imgLocation.substring(LogoPluginConst.IMGPATH_PREFIX_SOURCES.length());

      if ( !relativePath.startsWith("/")) {
        relativePath = "/" + relativePath;
      }

      String logoPath = baseDirPath + relativePath;

      File logo = new File(logoPath);
      InputStream inputStream = new FileInputStream(logo);

      try {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        copy(inputStream, buffer);
        logoByteArray = buffer.toByteArray();

      } finally {
        inputStream.close();
      }
    }
    return Base64.encodeBase64(logoByteArray);
  }

  /**
   * Gets image extension
   */
  protected final String getImgExt() {
    String imgExt = null;
    String imgLocation = settings.getString(LogoPluginConst.CONFKEY_IMG_LOCATION);

    if ( !StringUtils.isBlank(imgLocation)) {
      int dotPos = imgLocation.lastIndexOf('.');
      int len = imgLocation.length();
      if (dotPos >= 0 && len > dotPos + 1) {
        imgExt = imgLocation.substring(dotPos + 1, len);
      }
    }

    return imgExt;
  }
}