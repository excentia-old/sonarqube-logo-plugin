/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.logo;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metric.ValueType;
import org.sonar.api.measures.Metrics;

/**
 * Metrics for the Sonar Logo Plugin
 */
public class LogoMetrics implements Metrics {

  /**
   * Binary data is stored in this metric
   */
  public static final Metric LOGO_DATA = new Metric.Builder("logo_data", "Logo binary image data", ValueType.DATA)
      .setDescription("Logo binary image data").setDirection(Metric.DIRECTION_NONE).setQualitative(false)
      .setDomain(CoreMetrics.DOMAIN_GENERAL).create();

  /**
   * File extension for the image
   */
  public static final Metric LOGO_EXT = new Metric.Builder("logo_ext", "Logo image file extension", Metric.ValueType.STRING)
      .setDescription("Logo image file extension").setDirection(Metric.DIRECTION_NONE).setQualitative(false)
      .setDomain(CoreMetrics.DOMAIN_GENERAL).create();

  /**
   * Gets the plugin metrics
   */
  public final List<Metric> getMetrics() {
    return Arrays.asList(LOGO_DATA, LOGO_EXT);
  }
}