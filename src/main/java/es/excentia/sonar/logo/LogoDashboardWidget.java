/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.logo;

import org.sonar.api.web.AbstractRubyTemplate;
import org.sonar.api.web.Description;
import org.sonar.api.web.RubyRailsWidget;
import org.sonar.api.web.UserRole;
import org.sonar.api.web.WidgetCategory;

/**
 * Logo Widget
 */
@UserRole(UserRole.USER)
@Description("Shows a simple logo")
@WidgetCategory("Logo")
public class LogoDashboardWidget extends AbstractRubyTemplate implements RubyRailsWidget {

  /**
   * Get Widget Id
   */
  @Override
  public final String getId() {
    return "logo";
  }

  /**
   * Get widget title
   */
  @Override
  public final String getTitle() {
    return "Logo";
  }

  /**
   * Get widget template path
   */
  @Override
  protected final String getTemplatePath() {
    return "/es/excentia/sonar/logo/logo_dashboard_widget.html.erb";
  }
}