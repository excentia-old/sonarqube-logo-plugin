/*
 * Sonar Logo Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.logo;

/**
 * Class that encapsulates all constant attributes for the Sonar Logo Plugin
 */
public final class LogoPluginConst {

  /**
   * Private constructor to hide utility class constructor
   */
  private LogoPluginConst() {
  }

  /**
   * Sonar Logo Plugin property: location for the image, it can be an URL or a path to a file in the source code
   */
  public static final String CONFKEY_IMG_LOCATION = "es.excentia.sonar.logo.imglocation";

  /**
   * You can set the image style with this property, ex: width:200px;
   */
  public static final String CONFKEY_IMG_STYLE = "es.excentia.sonar.logo.style";

  /**
   * You can set a link in the image to redirect to.
   */
  public static final String CONFKEY_LINK = "es.excentia.sonar.logo.link";

  /**
   * Alternative text for the image
   */
  public static final String CONFKEY_ALT_TEXT = "es.excentia.sonar.logo.alttext";

  /**
   * Internal prefix to specify a image in the sources directory
   */
  public static final String IMGPATH_PREFIX_SOURCES = "sources:";
}