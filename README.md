# SonarQube Logo Plugin #

This plugin allows you to include an image inside a widget in your SonarQube dashboard.

### Features ###

You can include your project logo, developer's photo or any other image to improve the look of your dashboard project page.

You can show your images by linking to an existing URL (http://myapp/imgs/logo.png) or using a image located inside your project source code (sources:/src/main/site/logo.png).

### Download and product site ###

[Check out the product page to download the latest version](http://www.excentia.es/plugins/logo/descargar_en.html)
